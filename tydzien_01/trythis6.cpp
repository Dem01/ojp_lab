#include "std_lib_facilities.hpp"

int main(){
    constexpr double yen = 0.0090;
    constexpr double euros = 1.14;
    constexpr double pounds = 1.33;
    double value = 1;
    char unit = ' ';
    cout << "yen, euros and pounds to dollars converter \n";
    cout << "Please enter value and (y) for yen, (e) for euros or (d) for dollars " << endl;
    cin >> value >> unit;
    if (unit == 'y')
        cout << value << "yen(s) == " << value*yen << "dollar(s)" << endl;
    else if (unit == 'e')
        cout << value << "euro(s) == " << value*euros  << "dollar(s)" <<endl;
    else if (unit == 'p')
        cout << value << "pound(s) == " << value*pounds << "dollar(s)" <<endl;    
    else
        cout << "Sorry i don't know a unit called '" << unit << endl;


}
