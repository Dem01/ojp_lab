#include "std_lib_facilities.hpp"

int main(){
    
    char friend_sex = 0;

    cout << "Enter the name of the person you want to write to\n";
    string first_name;    
    cin >> first_name;
   
    int recipient_age = 0;
    cout << "Enter the age of the recipient\n";
    cin >> recipient_age;
    if (recipient_age > 110 or recipient_age < 0){
        simple_error("You're kidding!");
    }

    cout << "Enter the name of another friend\n";
    string friend_name;
    cin >> friend_name;
    
    cout << "Is your friend male or female ? (type m for male and f for female)\n";
    cin >> friend_sex;

    cout << "Dear " <<first_name<< ",\n";
    cout << "How are you? I am fine.\n";
    cout << "I hear you just had a birthday and you are " << recipient_age << "years old.";
    if (recipient_age < 12){
        cout << " Next year you will be " << recipient_age+1 << ".\n";
    }
    if (recipient_age == 17){
        cout << " Next year you will be able to vote.\n";
    }
    if (recipient_age > 70){
        cout << " I hope you're enjoying retirement.\n";
    }
    cout << "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec sollicitudin justo. Praesent vel porttitor urna. Mauris consectetur tempor lacus, vitae aliquam dui sollicitudin eget. Morbi aliquet tincidunt iaculis. Donec varius massa eu leo convallis, quis facilisis neque porttitor. Phasellus sit amet lacus a urna molestie faucibus et id sapien. Ut vulputate ornare felis, vitae congue purus elementum quis. Sed ex sapien, porttitor sed nunc vitae, vestibulum tempor tortor. Nunc bibendum sodales consequat. \n";
    
    cout << "Have you seen " << friend_name << " lately?\n";
    if(friend_sex == 'm'){
        cout << "If you see " << friend_name << " please ask him to call me.\n";
    }
    if(friend_sex == 'f'){
         cout << "If you see " << friend_name << " please ask her to call me.\n";
    }
    cout << "I miss you.\n";

    cout << "Yours sincerely\n\n";
    cout << "NAME\n";

    return 0;

}
