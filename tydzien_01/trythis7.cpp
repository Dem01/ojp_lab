#include "std_lib_facilities.hpp"

int main(){
    constexpr double yen = 0.009;
    constexpr double euros = 1.14;
    constexpr double pounds = 1.33;
    constexpr double yuan = 0.15;
    constexpr double kronar = 0.11;
    char a = ' ';
    cout << "Please enter how many dollars you want to convert: ";
    double value = 1;
    cin >> value;
    cout << "Convert to:\ny for yen\ne for euro\np for pound\nu for yuan\nk for kroner" << endl;
    cout << ">>";
    cin >> a;
    cout << "\n";
    switch(a){
        case 'y':
            cout << value << " dollar(s) == " << value*yen << "yen(s)\n";
            break;
        case 'e':
            cout << value << " dollar(s) == " << value*euros << "euro(s)\n";
            break;
        case 'p':
            cout << value << " dollar(s) == " << value*pounds << "pound(s)\n";
            break;
        case 'u':
            cout << value << " dollar(s) == " << value*yuan << "yuan(s)\n";
            break;
        case 'k':
            cout << value << " dollar(s) == " << value*kronar << "kronar(s)\n";
            break;
    }
}
