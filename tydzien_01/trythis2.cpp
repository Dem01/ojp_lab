#include "std_lib_facilities.hpp"

int main(){
    cout << "Please enter an integer value: ";
    int n;
    cin >> n;
    cout << "n == " << n
         << "\nn+1 == " << n+1
         << "\nthree times n == " << 3*n
         << "\ntwice n == " << n+n
         << "\nn squared == " << n*n
         << "\nn divided by 5 == " << n/5
         << "\nn modulo 2 == " << n%2
         // << "\nsquare root of n == " << sqrt(n)
         << '\n';
}
