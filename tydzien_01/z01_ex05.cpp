#include </home/dem/Pulpit/git/ojp_labo/std_lib_facilities.hpp>


int sum(int v1, int v2){
    return v1 + v2;
}

int diff(int v1, int v2){
    return v1 - v2;
}

int product(int v1, int v2){
    return v1 * v2;
}

double quotient(double v1, double v2){
    return v1 / v2;
}

int larger(int v1, int v2){
    if(v1 > v2){
        return v1;
    }else{
        return v2;
    }
}

int smaller(int v1, int v2){
    if(v1 < v2){
        return v1;
    }else{
        return v2;
    }
}

int main(){

    double val1 = 0;
    double val2 = 0;

    cout << "First floating-point value: ";
    cin >> val1;
    cout << "Second floting-point  value: ";
    cin >> val2;

    cout << "Sum of " << val1 << " and " << val2 << " = " << sum(val1, val2) << "\n";
    cout << "Diff of " << val1 << " and " << val2 << " = " << diff(val1, val2) << "\n";
    cout << "Product of " << val1 << " and " << val2 << " = " << product(val1, val2) << "\n";
    cout << "Quotient of " << val1 << " and " << val2 << " = " << quotient(val1, val2) << "\n";
    
    cout << "Smaller value is " << smaller(val1, val2) << "\n";
    cout << "larger value is " << larger(val1, val2) << "\n";

    return 0;
}
