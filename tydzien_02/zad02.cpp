#include "std_lib_facilities.hpp"

int main() {
  try {
    vector<int> numbers;
    int number = 0;
    int amount = 0;
    int sum = 0;
    cout << "Please enter the number of values you want to sum:";
    cin >> amount;
    while(cin >> number){

      numbers.push_back(number);
    }
    if (numbers.size() < amount)
      error("Type more values..");
    for(int i=0;i<amount;++i){
      sum += numbers[i];
    }
    cout << sum << endl;
    return 0;
  }
  catch(exception& e){
    cerr << e.what();
    return 1;
  }
  catch(...){
    cerr << "Unknown exception" << endl;
    keep_window_open();
    return 2;
  }

}
