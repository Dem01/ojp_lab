#include "std_lib_facilities.hpp"
int area(int length, int width){
  return length*width;
}


int main() {
  int s1 = area(7; // ) missing
  int s2 = area(7) // ; missing
  int s3 = area(7); // int is not a type
  int s4 = area('7); // ' not terminateed'
  return 0;
}
