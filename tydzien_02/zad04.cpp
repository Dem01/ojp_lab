#include "std_lib_facilities.hpp"

int factorial(int num){
  int factor = num;
  if (factor == 0){
    factor = 1;
  }
  else{
    for(int i = factor - 1; i > 0; --i){
      factor *= i;
    }
  }
  return factor;
}

int permutations(int num1, int num2){
  return factorial(num1)/factorial(num1-num2);
}

int combinations(int num1, int num2){
  return permutations(num1,num2)/factorial(num2);
}



int main(){
    try {
    cout << "Enter two numbers and 'p' if u want to calculate permutations"
        << "or 'c' if you want to calculate combinations\n";

    int num1, num2;
    char choice;

    cin >> num1 >> num2 >> choice;
    switch(choice){
      case 'p':
        cout <<"Permutations of " << num1 << "and" << num2 << "==" << permutations(num1,num2);
        break;
      case 'c':
        cout <<"Combinations of " << num1 << "and" << num2 << "==" << combinations(num1,num2);
        break;
      default:
        error("I do not know the command");
    }
    return 0;
  }


  catch(exception& e){
    cerr << e.what();
    return 1;
  }

  catch(...){
    cerr << "Something went wrong...";
    return 2;
  }
}
